from flask import Blueprint, request
from .user import User
from simple_project import db

rot = Blueprint('users', __name__)


@rot.route('/user/<int:user_id>', methods=['GET'])
def get(user_id):
    try:
        user = User.query.filter(User.id == user_id).first()
    except Exception:
        raise
    finally:
        db.session.close()

    return 'buscando um usuário ' + str(user.name)


@rot.route('/user', methods=['POST'])
def post():

    json = request.get_json()
    try:
        exist = User.query.filter(User.email == json['email']).first()

        if not exist:
            user = User(name=json['name'],
                        email=json['email'], password=json['password'])
            db.session.add(user)
            db.session.commit()
        else:
            return dict(data="Usuário já existe!")

    except Exception:
        db.session.rollback()
        raise
    finally:
        db.session.close()

    return dict(data=[user], status='OK')
