from simple_project import db, bcrypt
import datetime
import jwt


class User(db.Model):
    __tablename__ = 'use_user'

    id = db.Column(db.Integer, nullable=False,
                   primary_key=True, autoincrement=True)
    email = db.Column(db.String(200), nullable=False, unique=True)
    name = db.Column(db.String(200), nullable=False, unique=True)
    password = db.Column(db.String(256), nullable=False)
    register_on = db.Column(db.DateTime, nullable=False)
    admin = db.Column(db.Boolean, nullable=False)

    def __init__(self, name, email, password, admin=False):
        self.name = name
        self.email = email
        self.admin = admin
        self.register_on = datetime.datetime.now()
        self.password = bcrypt.generate_password_hash(password).decode('utf-8')

    def __repr__(self):
        return '<User  name: {} email: {}'.format(self.name, self.email)
