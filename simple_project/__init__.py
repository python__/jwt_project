from flask import Flask
from flask_bcrypt import Bcrypt
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
bcrypt = Bcrypt()


def create_app():
    app = Flask(__name__)

    app.config.from_object('config.config.Config')
    db.init_app(app)
    bcrypt.init_app(app)
    with app.app_context():
        # create tables
        from .users.user import User
        db.create_all()
        db.session.commit()

        from .users.routes import rot
        app.register_blueprint(users.routes.rot, url_prefix='/api/v1')
        return app

